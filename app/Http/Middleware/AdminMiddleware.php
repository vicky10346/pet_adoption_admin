<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // check if the admin is verified to enter
        if(Auth::check()){
            if(Auth::user()->role_as == '1'){
                // 1-Admin && 0-User
                // return "Hi admin!";
               return $next($request);
            }else{
                return redirect('accessdenied');
            }
        }
        // when you are not logged in
        else{
            return redirect ('/login')->with('message','Login is required.');
        }
        
    }
}
