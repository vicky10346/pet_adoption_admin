<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\Pet;

class DashboardController extends Controller
{
    public function index(){
        $users = User::where('role_as','0')->get();
        $announcements = Announcement::where('status','1')->get();
        $pets_available = Pet::where('pet_availability','1')->get();
        return view('admin.dashboard',compact('users','announcements','pets_available'));
    }
}
