<?php

namespace App\Http\Controllers\Admin;


use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class AnnouncementController extends Controller
{
    public function index()
    {
        $announcements = Announcement::all();
        return view('admin.announcement.index', compact('announcements'));
    }

    public function create()
    {
        return view('admin.announcement.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jfif,jpg,jpeg,png,gif|max:10240'
        ]);

        $announcement = new Announcement;

        if($request->hasfile('image')){
            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('uploads/announcements/', $filename);
            $announcement->image = $filename;
        }


        $announcement->title = $request->input('title');
        $announcement->description = $request->input('description');
        $announcement->status = $request->status == true ? '1' : '0';

        $announcement->save();
        return redirect('admin/announcements')->with('message', 'Announcement is added with success!');
    }

    public function edit($id)
    {
        $announcement = Announcement::find($id);
        return view('admin.announcement.edit', compact('announcement'));
    }

    public function update(Request $request, $id)
    {
        $announcement = Announcement::find($id);

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'nullable|mimes:jfif,jpg,jpeg,png,gif|max:10240'
        ]);

        if ($request->hasfile('image')) {
            $destination = 'uploads/announcements/' . $announcement;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('uploads/announcements/', $filename);
            $announcement->image = $filename;
            
        }
        $announcement->title = $request->input('title');
        $announcement->description = $request->input('description');
        $announcement->status = $request->status == true ? '1' : '0';

        $announcement->update();
        return redirect('admin/announcements')->with('message', 'Announcement has been updated with success!');
    }

    public function destroy($id)
    {
        $announcement = Announcement::find($id);

        if ($announcement) {
            $destination = '/storage/' . $announcement;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $announcement->delete();
            return redirect('admin/announcements')->with('message', 'Announcement has been removed with success!');
        }
    }
}
