<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class PetController extends Controller
{
    public function index()
    {
        $pets = Pet::all();
        return view('admin.pet.available.index', compact('pets'));
        
    }

    public function create()
    {
        return view('admin.pet.available.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'pet_name' => 'required', 
            'pet_category' => 'required',
            'pet_age' => 'required',
            'pet_age_category' => 'required',
            'pet_gender' => 'required',
            'pet_breed' => 'required',
            'pet_fur_colour' => 'required',
            'pet_description' => 'required',
            'pet_image' => 'nullable|mimes:jfif,jpg,jpeg,png,gif|max:10240'

        ]);

        $pet = new Pet;

        $pet->pet_name = $request->input('pet_name');
        $pet->pet_category = $request->get('pet_category');
        $pet->pet_age = $request->input('pet_age');
        $pet->pet_age_category = $request->get('pet_age_category');
        $pet->pet_gender = $request->get('pet_gender');
        $pet->pet_breed = $request->input('pet_breed');
        $pet->pet_fur_colour = $request->input('pet_fur_colour');
        $pet->pet_description = $request->input('pet_description');

        $file = $request->file('pet_image');
        $filename = time().'.'.$file->getClientOriginalExtension();
        $file->move('uploads/available-pets/', $filename);
        $pet->pet_image = $filename;

        $pet->pet_availability = $request->pet_availability == true ? '1' : '0';
        $pet->pet_adoption_status = $request->pet_adoption_status == true ? '1' : '0';

        $pet->save();
        return redirect('admin/pet/available')->with('message', 'Pet is added with success!');
    }

    public function edit($id)
    {
        $pet = Pet::find($id);
        return view('admin.pet.available.edit', compact('pet'));
    }

    public function update(Request $request, $id)
    {
        $pet = Pet::find($id);

        $request->validate([
            'pet_name' => 'required', 
            'pet_category' => 'required',
            'pet_age' => 'required',
            'pet_age_category' => 'required',
            'pet_gender' => 'required',
            'pet_breed' => 'required',
            'pet_fur_colour' => 'required',
            'pet_description' => 'required',
            'pet_image' => 'nullable|mimes:jfif,jpg,jpeg,png,gif|max:10240'
        ]);

        $pet->pet_name = $request->input('pet_name');
        $pet->pet_category = $request->get('pet_category');
        $pet->pet_age = $request->input('pet_age');
        $pet->pet_age_category = $request->get('pet_age_category');
        $pet->pet_gender = $request->get('pet_gender');
        $pet->pet_breed = $request->input('pet_breed');
        $pet->pet_fur_colour = $request->input('pet_fur_colour');
        $pet->pet_description = $request->input('pet_description');

        if ($request->hasfile('pet_image')) {
            $destination = '/storage/' . $pet;
            if (File::exists($destination)) {
                File::delete($destination);
            }

            $file = $request->file('pet_image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('uploads/available-pets/', $filename);
            $pet->pet_image = $filename;
        }

        $pet->pet_availability = $request->pet_availability == true ? '1' : '0';
        $pet->pet_adoption_status = $request->pet_adoption_status == true ? '1' : '0';

        $pet->update();
        return redirect('admin/pet/available')->with('message', 'Pet has been updated with success!');
    }

    public function destroy($id)
    {
        $pet = Pet::find($id);

        if ($pet) {
            $destination = 'uploads/available-pets/' . $pet;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $pet->delete();
            return redirect('admin/pet/available')->with('message', 'Pet has been removed with success!');
        }
    }
}
