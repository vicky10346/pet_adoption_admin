<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pet;
use Illuminate\Http\Request;

class AdoptedPetController extends Controller
{
    public function index(){
        $adopted_pets = Pet::where('pet_adoption_status','1')->get();
        return view('admin.pet.adopted.index',compact('adopted_pets'));
    }
    
}
