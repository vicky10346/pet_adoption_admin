<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\CompanyDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class CompanyDetailController extends Controller
{
    public function index()
    {
        $companydetails = CompanyDetail::all();
       return view ('admin.companydetail.index',compact('companydetails'));
    }

    public function edit($companydetail_id)
    {
        $companydetail = CompanyDetail::find($companydetail_id);
        return view('admin.companydetail.edit',compact('companydetail'));
    }

    public function update(Request $request, $companydetail_id)
    {
        $companydetail= CompanyDetail::find($companydetail_id);

        $request->validate([
            'logo' => 'required|mimes:jpeg,png,gif,jfif|max:10240',
            'name' => 'required',
            'title'=>'required',
            'description'=>'required',
            'email' => 'required',
            'address'=>'required',
            'contact_no'=>'required|regex:/(60)[0-9]{9}/'
        ]);

        if ($request->hasfile('logo')) {
            $destination = 'uploads/company/' . $companydetail;
            if (File::exists($destination)) {
                File::delete($destination);
            }

            $file = $request->file('logo');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $file->move('uploads/company/', $filename);
            $companydetail->logo = $filename;
        }

        $companydetail->name = $request->input('name');
        $companydetail->title = $request->input('title');
        $companydetail->description = $request->input('description');
        $companydetail->email = $request->input('email');
        $companydetail->address = $request->input('address');
        $companydetail->contact_no = $request->input('contact_no');

        $companydetail->update();
        return redirect('admin/compdetails')->with('message', 'Company details has been updated sucessfully!');

    }
}
