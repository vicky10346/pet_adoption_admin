<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdoptionRequest;
use Illuminate\Http\Request;

class AdoptionRequestController extends Controller
{
    public function index()
    {
        $requests = AdoptionRequest::all();
       return view('admin.adoptionrequest.index',compact('requests'));
    }
}
