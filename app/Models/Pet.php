<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    use HasFactory;

    protected $table = 'pets';

    protected $fillable = [
        'pet_name',
        'pet_age',
        'pet_breed',
        'pet_fur_colour',
        'pet_description',
        'pet_image',
        'pet_availability',
        'pet_adoption_status'  

    ];
}
