<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav nav-pills">
                <img src="{{ asset('images/logo_default.png') }}" alt="logo" class="img-fluid">
                <div class="sb-sidenav-menu-heading">Menu Navigation</div>
                <a class="nav-link {{ Request::is('admin/dashboard') ? 'active' : '' }}"
                    href="{{ url('admin/dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                <a class="nav-link {{ Request::is('admin/compdetails') ? 'active' : '' }}" href="{{ url('admin/compdetails') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-building"></i></div>
                    Company Details
                </a>
                

                <a class="nav-link {{ Request::is('admin/users') ? 'active' : '' }}" href="{{ url('admin/users') }}" >
                    <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                    Users
                </a>

                <a class="nav-link {{ Request::is('admin/comments') ? 'active' : '' }}" href="{{ url('admin/comments') }}" >
                    <div class="sb-nav-link-icon"><i class="fas fa-comments"></i></div>
                    Users Comments
                </a>

                <a class="nav-link {{ Request::is('admin/announcements') ||Request::is('admin/create-announcement') ||Request::is('admin/edit-announcement/*')? 'active' : '' }}" href="{{ url('admin/announcements') }}" >
                    <div class="sb-nav-link-icon"><i class="fas fa-calendar"></i></div>
                    Announcements
                </a>

                <a class="nav-link {{ Request::is('admin/pet/available') ||Request::is('admin/create-pet') ||Request::is('admin/edit-pet/*')? 'active' : '' }}" href="{{ url('admin/pet/available') }}" >
                    <div class="sb-nav-link-icon"><i class="fas fa-paw"></i></div>
                    Pets
                </a>

                <a class="nav-link {{ Request::is('admin/adoptrequest') ? 'active' : '' }}" href="{{ url('admin/adoptrequest') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-envelope"></i></div>
                    Adoption Request
                </a>

            </div>
        </div>

    </nav>
</div>
