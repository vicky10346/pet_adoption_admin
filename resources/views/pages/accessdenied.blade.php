@extends('layouts.app')
@section('title','Access Denied')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="text-center mt-4">
                <img class="mb-4 img-error" src="{{asset('images/404.jpg')}}" class="img-fluid"/>
                <p class="lead">Sorry, this page is accessible to admin only.</p>
                <a href="{{ route('logout') }} ">
                    <i class="fas fa-arrow-left me-1"></i>
                    Return to Logout
                </a>
            </div>
        </div>
    </div>
</div>
@endsection