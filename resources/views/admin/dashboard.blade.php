@extends('layouts.adminMaster')
@section('title', 'Dashboard')
@section('content')
    <div class="container-fluid px-4">
        <h1>Dashboard</h1>

        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="card bg-info text-white mb-4">
                    <div class="card-body">
                        <h6>Users</h6>
                        <div class="custom-icon">
                            <i class="fa-solid fa-users fa-3x"></i>
                        </div>
                        <h5>{{ $users->count() }} </h5>

                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-dark stretched-link text-decoration-none" href="{{ url('admin/users') }}">View Details</a>
                        <div class="small text-dark"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card bg-info text-white mb-4">
                    <div class="card-body">
                        <h6>Announcements</h6>
                        <div class="custom-icon">
                            <i class="fa-solid fa-bullhorn fa-3x"></i>
                        </div>
                        <h5>{{ $announcements->count() }} </h5>

                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-dark stretched-link text-decoration-none" href="{{ url('admin/announcements') }}">View Details</a>
                        <div class="small text-dark"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card bg-info text-white mb-4">
                    <div class="card-body">
                        <h6>Pets Available</h6>
                        <div class="custom-icon">
                            <i class="fa-solid fa-cat fa-2x"></i>
                            <i class="fa-solid fa-dog fa-3x"></i>
                        </div>
                        <h5>{{ $pets_available->count() }} </h5>

                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-dark stretched-link text-decoration-none" href="{{ url('admin/pet/available') }}">View Details</a>
                        <div class="small text-dark"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card bg-info text-white mb-4">
                    <div class="card-body">
                        <h6>Adoption Requests</h6>
                        <div class="custom-icon">
                            <i class="fa-solid fa-envelope fa-2x"></i>
                        </div>
                        <h5>1</h5>

                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-dark stretched-link text-decoration-none" href="#">View Details</a>
                        <div class="small text-dark"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
