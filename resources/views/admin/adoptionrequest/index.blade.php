@extends('layouts.adminMaster')
@section('title','Adoption Requests')
@section('content')
        <div class="container-fluid px-4">
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Adoption Requests</h2>
                </div>
                <div class="card-body table-responsive">
                    @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    <table id="mainDataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>Request ID</th>
                            <th>User ID</th>
                            <th>Pet ID</th>
                            <th>Pet Name</th>
                            <th>Pet Adoption Status</th>
                            <th>Reuqest status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($requests as $requestItem)
                             <tr class="text-center"> 
                                  <td>{{ $requestItem->id }}</td> 
                                  <td>{{ $requestItem->user_id }}</td> 
                                  <td>{{ $requestItem->pet_id }}</td> 
                                  <td>{{ $requestItem->pet_name }}</td>
                                  <td>{{ $requestItem->pet_adoption_status }}</td> 
                                  <td><span class= "{{ $requestItem->request_status =='1' ? 'badge bg-success': 'badge bg-danger' }}">{{$requestItem->request_status == '1' ? 'Approved':'Rejected'}}</span></td> 
                            @endforeach
                           

                           </tr>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>     
@endsection
