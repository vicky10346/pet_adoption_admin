@extends('layouts.adminMaster')
@section('title', 'Edit Announcement')
@section('content')
    <div class="container-fluid">
        <div class="card mt-4">
            <div class="card-header">
                <h2>Edit company details</h2>
            </div>
            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                </div>
                @endif  
                
                <form action="{{ url('admin/update-compdetail/'.$companydetail->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="name">Company Name</label>
                        <input type="text" name="name" value="{{$companydetail->name}}" id="name" class="form-control" >
                    </div>

                    <div class="mb-3">
                        <label for="title">Company Header Title</label>
                        <input type="text" name="title" value="{{$companydetail->title}}" id="title" class="form-control" >
                    </div>

                    <div class="mb-3">
                        <label for="description">Company Header Description</label>
                        <textarea type="text" name="description" id="editor" rows="5"
                                  class="form-control" > {!! $companydetail->description !!} </textarea>
                    </div>

                    <div class="mb-3">
                        <label for="email">Company Email</label>
                        <input type="email" name="email" value="{{$companydetail->email}}" id="email" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="contact_no">Company Contact Number
                        </label>
                        <p class="text-danger">*
                            <span class="text-small text-muted">Please start with (60)+</span>
                        </p>
                       
                        <input type="text" name="contact_no" value="{{$companydetail->contact_no}}" id="contact_no" class="form-control" >
                    </div>

                    <div class="mb-3">
                        <label for="name">Company Address</label>
                        <textarea type="text" name="address" id="addressr" rows="3"
                                  class="form-control"> {!! $companydetail->address !!} </textarea>
                    </div>
                  
                    <div class="mb-3">
                        @if ($companydetail->logo)
                            <img src="{{asset('/uploads/company/'.$companydetail->logo)}}" class="img-fluid w-50" alt="submitted-logo-image">
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="formFile">Logo Image</label>
                        <input class="form-control" type="file" accept="image/*" id="logo" name="logo">
                    </div>

                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                          Update
                        </button>
                        <a href="{{url('admin/compdetails')}}" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
