@extends('layouts.adminMaster')
@section('title', 'Announcements')
@section('content')
    <div class="container-fluid px-4">
        <div class="card mt-4">
            <div class="card-header">
                <h2>Company Details</h2>
            </div>
            <div class="card-body table-responsive">
                @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                @endif
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>Company logo</th>
                            <th>Company name</th>
                            <th>Company header title</th>
                            <th>Company description</th>
                            <th>Company email</th>
                            <th>Company address</th>
                            <th>Company contact info</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($companydetails as $detail)
                            <tr class="justify-content-center text-center">
                                <td>
                                    <img src="{{asset('/uploads/company/'.$detail->logo)}}" alt="logo" class="img-fluid w-50">
                                </td>
                                <td>{{ $detail->name }}</td>
                                <td>{{$detail->title}}</td>
                                <td>{!! $detail->description !!}</td>
                                <td>{{ $detail->email }}</td>
                                <td>{{ $detail->address }}</td>
                                <td>{{ $detail->contact_no }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ url('admin/edit-compdetail/'.$detail->id) }}" class="btn btn-warning btn-sm" title="Edit"><i class="fa-solid fa-pen-to-square"></i></a>    
                                    </div>
                                </td>
                        @endforeach
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </div>
@endsection
