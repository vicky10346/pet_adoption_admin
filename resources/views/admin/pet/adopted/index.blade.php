@extends('layouts.adminMaster')
@section('title','Adopted Pets')
@section('content')
        <div class="container-fluid px-4">
           
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Adopted Pets
                        <a href="{{url('admin/pet/available/create-pet')}}"> <button type="button" class="btn btn-secondary float-end" title="Add new announcement"><i class="fa-solid fa-circle-plus"></i> Add New Pet</button></a>
                    </h2>
                </div>
                <div class="card-body table-responsive">
                    @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    <table id="mainDataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>Pet ID</th>
                            <th>Pet Name</th>
                            <th>Pet Description</th>
                            <th>Pet Image</th>
                            <th>Pet Availability</th>
                            <th>Pet Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($adopted_pets as $pet)
                            <tr class="text-center">
                                <td>{{$pet->id}}</td>
                                <td>{{$pet->pet_name}}</td>
                                <td>{$pet->pet_description}</td>
                                <td>
                                    <img src="{{ asset('/uploads/available-pets/'.$pet->pet_image) }}" alt="pet-images-stored" class="img-fluid w-75">
                                </td>
                                <td>{{$pet->pet_availability == '1' ? 'Shown':'Hidden'}}</td>
                                <td>{{$pet->pet_status == '1' ? 'Not Adopted':'Adopted'}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ url('admin/edit-pet/' . $pet->id) }}"
                                            class="btn btn-warning btn-sm" title="Edit"><i class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="{{ url('admin/delete-pet/' . $pet->id) }}"
                                            class="btn btn-danger btn-sm" onclick="return confirm('Remove the pet?')" title="Remove"><i class="fa-solid fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                                
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>     
@endsection
