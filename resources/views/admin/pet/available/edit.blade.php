@extends('layouts.adminMaster')
@section('title', 'Edit Pet Details')
@section('content')
    <div class="container-fluid">
        <div class="card mt-4">
            <div class="card-header">
                <h2>Edit pet</h2>
                <p class="lead">Either edit or modify pet of your choice.</p>
            </div>
            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                </div>
                @endif  
                
                <form action="{{ url('admin/update-pet/'.$pet->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="name">Pet Name</label>
                        <input type="text" name="pet_name" value="{{$pet->pet_name}}" id="pet_name" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="">Pet Category</label>
                            <select name="pet_category" class="form-control">
                                <option value="">--Select Category--</option>
                                <option value="dog"@if($pet->pet_category=='dog') selected='selected' @endif >Dog</option>
                                <option value="cat"@if($pet->pet_category=='cat') selected='selected' @endif >Cat</option>
                                <option value="other"@if($pet->pet_category=='other') selected='selected' @endif >Other</option>
                            </select>
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Age</label>
                        <input type="text" name="pet_age" value="{{$pet->pet_age}}" id="pet_age" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="">Pet Age Category</label>
                            <select name="pet_age_category" class="form-control">
                                <option value="">--Select Category--</option>
                                <option value="young"@if($pet->pet_age_category=='young') selected='selected' @endif >Young</option>
                                <option value="adult"@if($pet->pet_age_category=='adult') selected='selected' @endif >Adult</option>
                                <option value="old"@if($pet->pet_age_category=='old') selected='selected' @endif >Old</option>
                            </select>
                    </div>

                    <div class="mb-3">
                        <label for="">Pet Gender</label>
                            <select name="pet_gender" class="form-control">
                                <option value="">--Select Gender--</option>
                                <option value="Male"@if($pet->pet_gender=='Male') selected='selected' @endif >Male</option>
                                <option value="Female"@if($pet->pet_gender=='Female') selected='selected' @endif >Female</option>
                            </select>
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Breed</label>
                        <input type="text" name="pet_breed" value="{{$pet->pet_breed}}" id="pet_breed" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Fur Colour</label>
                        <input type="text" name="pet_fur_colour" value="{{$pet->pet_fur_colour}}" id="pet_fur_colour" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Description</label>
                        <input type="text" name="pet_description" value="{{$pet->pet_description}}" id="pet_description" class="form-control">
                    </div>

                    <div class="mb-3">
                        @if ($pet->pet_image)
                            <img src="{{ asset('/uploads/available-pets/'.$pet->pet_image) }}" alt="pet-images-stored" class="img-fluid w-75">
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="formFile">Pet Image</label>
                        <input class="form-control" type="file" accept="image/*" id="pet_image" name="pet_image">
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Availability (check to display)</label>
                            <input type="checkbox" {{$pet->pet_availability == '1' ? 'checked':''}} name="pet_availability" id="pet_availability">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Adoption Status (check if adopted)</label>
                            <input type="checkbox" {{$pet->pet_adoption_status == '1' ? 'checked':''}} name="pet_adoption_status" id="pet_adoption_status">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                          Update
                        </button>
                        <a href="{{url('admin/pet/available')}}" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
