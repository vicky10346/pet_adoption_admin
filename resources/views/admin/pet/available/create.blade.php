@extends('layouts.adminMaster')
@section('title', 'Pet')
@section('content')
    <div class="container-fluid">
        <div class="card mt-4">
            <div class="card-header">
                <h2>Add Pet</h2>
                <p class="lead card-subtitle">Add a new pet.</p>
            </div>
            <div class="card-body"> 
                @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                </div>
                @endif   
                <form action="{{ url('admin/store-pet') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="mb-3">
                        <label for="name">Pet Name</label>
                        <input type="text" name="pet_name" id="pet_name" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="">Pet Category</label>
                            <select name="pet_category" class="form-control">
                                <option value="">--Select Category--</option>
                                <option value="dog">Dog</option>
                                <option value="cat">Cat</option>
                                <option value="other">Other</option>
                            </select>
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Age</label>
                        <input type="text" name="pet_age" id="pet_age" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="">Pet Age Category</label>
                            <select name="pet_age_category" class="form-control">
                                <option value="">--Select Age Category--</option>
                                <option value="young">Young</option>
                                <option value="adult">Adult</option>
                                <option value="old">Old</option>
                            </select>
                    </div>

                    <div class="mb-3">
                        <label for="">Gender</label>
                            <select name="pet_gender" class="form-control">
                                <option value="">--Select Gender--</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Breed</label>
                        <input type="text" name="pet_breed" id="pet_breed" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Fur Colour</label>
                        <input type="text" name="pet_fur_colour" id="pet_fur_colour" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="name">Pet Description</label>
                        <input type="text" name="pet_description" id="pet_description" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="formFile">Pet Image</label>
                        <input class="form-control" type="file" accept="image/*" id="pet_image" name="pet_image">
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Availability (check to display)</label>
                            <input type="checkbox" name="pet_availability" id="pet_availability">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Adoption Status (check if adopted)</label>
                            <input type="checkbox" name="pet_adoption_status" id="pet_adoption_status">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                            Save
                        </button>
                        <a href="{{url('admin/available')}}" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
