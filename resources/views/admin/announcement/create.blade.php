@extends('layouts.adminMaster')
@section('title', 'Announcement')
@section('content')
    <div class="container-fluid">
        <div class="card mt-4">
            <div class="card-header">
                <h2>Add announcements</h2>
                <p class="lead card-subtitle">Add announcements made from the pet adoption center.</p>
            </div>
            <div class="card-body"> 
                @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                </div>
                @endif   
                <form action="{{ url('admin/store-announcement') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="mb-3">
                        <label for="name">Announcement Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
                    </div>
                    <div class="mb-3">
                        <label for="name">Announcement Description</label>
                        <textarea type="text" name="description" id="description" rows="5"
                                  class="form-control">{{old('title')}}</textarea>
                    </div>
                    <div class="mb-3">
                        <label for="formFile">Page Image Header</label>
                        <input class="form-control" type="file" accept="image/*" id="image" name="image">
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Status (check to display)</label>
                            <input type="checkbox" name="status" id="status">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                            Save
                        </button>
                        <a href="{{url('admin/announcements')}}" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
