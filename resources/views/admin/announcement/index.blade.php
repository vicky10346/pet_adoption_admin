@extends('layouts.adminMaster')
@section('title','Announcements')
@section('content')

        <div class="container-fluid px-4">
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Announcements
                        <a href="{{url('admin/create-announcement')}}"> <button type="button" class="btn btn-secondary float-end" title="Add new announcement"><i class="fa-solid fa-circle-plus"></i> New Announcement</button></a>
                    </h2>
                </div>
                <div class="card-body table-responsive">
                    @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    <table id="mainDataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>Announcement ID</th>
                            <th>Announcement title</th>
                            <th>Announcement description</th>
                            <th>Announcement image</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($announcements as $announcement)
                            <tr class="text-center">
                                <td>{{$announcement->id}}</td>
                                <td>{{$announcement->title}}</td>
                                <td>{!! $announcement->description !!}</td>
                                <td>
                                    <img src="{{ asset('/uploads/announcements/'.$announcement->image) }}" alt="announcement-images-stored" class="img-fluid w-75">
                                </td>
                                <td>{{$announcement->status == '1' ? 'Shown':'Hidden'}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ url('admin/edit-announcement/' . $announcement->id) }}"
                                            class="btn btn-warning btn-sm" title="Edit"><i class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="{{ url('admin/delete-announcement/' . $announcement->id) }}"
                                            class="btn btn-danger btn-sm" onclick="return confirm('Remove the announcement?')" title="Delete"><i class="fa-solid fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                                
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>     
@endsection
