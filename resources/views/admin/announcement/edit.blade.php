@extends('layouts.adminMaster')
@section('title', 'Edit Announcement')
@section('content')
    <div class="container-fluid">
        <div class="card mt-4">
            <div class="card-header">
                <h2>Edit announcement</h2>
                <p class="lead">Either edit or modify announcements of your choice.</p>
            </div>
            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                    @endforeach
                </div>
                @endif  
                
                <form action="{{ url('admin/update-announcement/'.$announcement->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="name">Announcement Name</label>
                        <input type="text" name="title" value="{{$announcement->title}}" id="title" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="name">Announcement Description</label>
                        <textarea type="text" name="description" id="description" rows="5"
                                  class="form-control"> {{$announcement->description}} </textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Status (check to display)</label>
                            <input type="checkbox" {{$announcement->status == '1' ? 'checked':''}} name="status" id="status">
                        </div>
                    </div>

                    <div class="mb-3">
                        @if ($announcement->image)
                            <img src="{{asset('/uploads/announcements/'.$announcement->image)}}" class="img-fluid w-50" alt="submitted-page-header-image">
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="formFile">Announcement Image</label>
                        <input class="form-control" type="file" accept="image/*" id="image" name="image">
                    </div>
 

                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">
                          Update
                        </button>
                        <a href="{{url('admin/announcements')}}" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
