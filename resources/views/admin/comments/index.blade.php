@extends('layouts.adminMaster')
@section('title','Pets')
@section('content')
        <div class="container-fluid px-4">
            <a href="{{url('admin/create-post')}}"> <button type="button" class="btn btn-secondary">Add New Pet</button></a>
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Comments</h2>
                </div>
                <div class="card-body table-responsive">
                    @if (session('message'))
                    <div class="alert alert-success">{{ session('message') }}</div>
                    @endif
                    <table id="mainDataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>Comment ID</th>
                            <th>User ID</th>
                            <th>Pet ID</th>
                            <th>Pet name</th>
                            <th>Comment</th>
                          
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($comments as $comment)
                            <tr class="justify-content-center text-center">
                                <td>{{ $comment->id}}</td>
                                <td>{{ $comment->user_id}}</td>
                                <td>{{$comment->pet_id }}</td>
                                <td>{{ $comment->pet->pet_name }}</td>
                                <td>{!! $comment->comment !!}</td>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>     
@endsection
