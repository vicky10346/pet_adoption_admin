@extends('layouts.adminMaster')

@section('title', 'Users')

@section('content')
    <div class="container-fluid px-4">
        <div class="card mt-4">
            <div class="card-header">
                <h4>View User</h4>
            </div>
            <div class="card-body">
                <div class="mb-3">
                    <label for="username">Username</label>
                    <p class="form-control">{{ $user->name }}</p>
                </div>
                <div class="mb-3">
                    <label for="email">Email</label>
                    <p class="form-control">{{ $user->email }}</p>
                </div>
                <div class="mb-3">
                    <label for="name">Created at</label>
                    <p class="form-control">{{ $user->created_at->format('d/m/Y') }}</p>
                </div>

                <a href="{{url('admin/users')}}" class="btn btn-danger">
                    Cancel
                </a>

            </div>

        </div>
    </div>
@endsection
