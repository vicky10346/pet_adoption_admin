@extends('layouts.adminMaster')
@section('title','Dashboard')
@section('content')

        <div class="container-fluid px-4">
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Users</h2>
                </div>
                <div class="card-body table-responsive">
                    @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                    <table id="mainDataTable" class="table table-bordered table-striped">
                        <thead>
                        <tr class="text-center">
                            <th>User ID</th>
                            <th>Name</th>
                            <th>User email</th>
                            <th>User role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                        <tr class="text-center">
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role_as == '1' ? 'Admin' : 'User'}}</td>
                            
                            <td>
                                <div class="btn-group">
                                    <a href="{{ url('admin/view-user/'.$user->id) }}"  class="btn btn-primary btn-sm" title="View"><i class="fa-solid fa-eye"></i></a>    
                                    <a href="{{ url('admin/edit-user/'.$user->id) }}" class="btn btn-warning btn-sm" title="Edit"><i class="fa-solid fa-pen-to-square"></i></a>    
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

        </div>     
@endsection
