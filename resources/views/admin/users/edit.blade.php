@extends('layouts.adminMaster')

@section('title', 'Users')

@section('content')
    <div class="container-fluid px-4">
        <div class="card mt-4">
            <div class="card-header">
                <h4>Edit User</h4>
            </div>
            <div class="card-body">
                <div class="mb-3">
                    <label for="username">Username</label>
                    <p class="form-control">{{ $user->name }}</p>
                </div>
                <div class="mb-3">
                    <label for="email">Email</label>
                    <p class="form-control">{{ $user->email }}</p>
                </div>
                <div class="mb-3">
                    <label for="name">Created at</label>
                    <p class="form-control">{{ $user->created_at->format('d/m/Y') }}</p>
                </div>

                <form action="{{ url('admin/update-user/'.$user->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <label for="role_as">Select role</label>
                    <select name="role_as" class="form-control mb-3">
                        <option value="1" {{ $user->role_as == '1' ? 'selected' : '' }}>Admin</option>
                        <option value="0" {{ $user->role_as == '0' ? 'selected' : '' }}>User</option>
                        <option value="2" disabled>Editor</option>
                    </select>
                    <div class="mt-3 col-md-6">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                        <a href="{{url('admin/users')}}" class="btn btn-danger">
                            Cancel
                        </a>
                    </div>
                </form>

            </div>

        </div>
    </div>
@endsection
