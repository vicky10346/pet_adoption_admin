<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Admin\AnnouncementController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@index')->name('login');
Route::post('login','Auth\LoginController@store');

Route::get('logout','Auth\LogoutController@store');

Route::prefix('admin')->middleware(['auth','isAdmin'])->group(function(){
    // announcements
    Route::get('/dashboard','Admin\DashboardController@index');
    Route::get('/announcements','Admin\AnnouncementController@index');
    Route::get('/create-announcement','Admin\AnnouncementController@create');
    Route::post('/store-announcement','Admin\AnnouncementController@store');
    Route::get('/edit-announcement/{id}','Admin\AnnouncementController@edit');
    Route::put('/update-announcement/{id}','Admin\AnnouncementController@update');
    Route::get('/delete-announcement/{id}','Admin\AnnouncementController@destroy');

    //adoption request
    Route::get('/adoptrequest','Admin\AdoptionRequestController@index');

    // company details
    Route::get('/compdetails','Admin\CompanyDetailController@index');
    Route::get('/edit-compdetail/{id}','Admin\CompanyDetailController@edit');
    Route::put('/update-compdetail/{id}','Admin\CompanyDetailController@update');


    // users
    Route::get('users','Admin\UserController@index');
    Route::get('view-user/{user_id}','Admin\UserController@show');
    Route::get('edit-user/{user_id}','Admin\UserController@edit');
    Route::put('update-user/{user_id}','Admin\UserController@update');

    // comments
    Route::get('comments','Admin\CommentController@index');

    
    // available pets
    Route::get('/pet/available','Admin\PetController@index');
    Route::get('/pet/available/create-pet','Admin\PetController@create');
    Route::post('store-pet','Admin\PetController@store');
    Route::get('/edit-pet/{id}','Admin\PetController@edit');
    Route::put('/update-pet/{id}','Admin\PetController@update');
    Route::get('/delete-pet/{id}','Admin\PetController@destroy');

    //adopted pets
    Route::get('/pet/adopted','Admin\AdoptedPetController@index');

});
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// access denied for users
Route::get('/accessdenied', function(){
    return view('pages.accessdenied');
})->name('accessdenied');