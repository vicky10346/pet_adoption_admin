<?php

namespace Database\Seeders;

use App\Models\CompanyDetail;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanyDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_details')->insert([
            'logo'=> 'logo_default.png',
            'name' => 'Pet Adoption Project Company',
            'email' => 'loremipsum@gmail.com',
            'address'=>'123, Lebuh Gelugor, 11600 Penang, Malaysia.',
            'contact_no'=>'123456789'
        ]);
    }
}
