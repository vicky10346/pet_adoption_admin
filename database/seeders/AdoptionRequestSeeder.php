<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdoptionRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adoptionrequests')->insert([
            'user_id'=>'3',
            'pet_id'=>'3',
            'pet_name'=>'Oren',
            'pet_adoption_status'=>'1',
            'request_status'=>'1'
        ]);
    }
}
